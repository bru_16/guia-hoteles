$(function () {
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
  $('.carousel').carousel({
    interval: 3000
  })

  $('#contacto').on('show.bs.modal', function (e) {
    console.log("el modal #contacto se esta mostrando");
    $('#btnMagico').removeClass('btn-danger');
    $('#btnMagico').prop('disabled',true);
  });
  $('#contacto').on('shown.bs.modal', function (e) {
    console.log("el modal #contacto se mostró");
  });
  $('#contacto').on('hide.bs.modal', function (e) {
    console.log("el modal #contacto se oculta");
    $('#btnMagico').addClass('btn-danger');
    $('#btnMagico').prop('disabled',false);
  });
  $('#contacto').on('hidden.bs.modal', function (e) {
    console.log("el modal #contacto se ocultó");
  });

  $('.btn-reserva').click(function () {
    $(this).removeClass('btn-primary');
    $(this).css('background-color', 'grey');
    $(this).css('color', 'white');
  });
});
